let http = require("http");


http.createServer(function(request,response){

	console.log(request.url);
	console.log(request.method);

	/*
		HTTP Requests are differentiated not only via their endpoints but also with their methods

		HTPP methods simply tells the server what action it must take or what kind of response is needed for the request

		With an HTTP Method we can actually create routes with the same endpoint but with different methods

	*/

	if(request.url == "/" && request.method == "GET"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end('Welcome to booking system');

	}

	else if(request.url == "/profile" && request.method == "GET"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end('Welcome to your profile');


}

	else if(request.url == "/courses" && request.method == "GET"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end("Here's our available courses");


}
	else if(request.url == "/addCourse" && request.method == "POST"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end('Add course to our resources');


}	

	else if(request.url == "/updateCourse" && request.method == "PUT"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end('Update a course to our resources');


}

	else if(request.url == "/archiveCourse" && request.method == "DELETE"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end('Archive courses to our resources');


}

}).listen(4000);

console.log('Server running at localhost:4000');