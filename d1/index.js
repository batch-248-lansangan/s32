let http = require("http");


http.createServer(function(request,response){

	console.log(request.url);
	console.log(request.method);

	/*
		HTTP Requests are differentiated not only via their endpoints but also with their methods

		HTPP methods simply tells the server what action it must take or what kind of response is needed for the request

		With an HTTP Method we can actually create routes with the same endpoint but with different methods

	*/

	if(request.url == "/items" && request.method == "GET"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end('Data retrieved from the database!');

	}

	else if(request.url == "/items" && request.method == "POST"){

			//Requests the "/items" path and "GETS' infromation
			response.writeHead(200, {'Content-Type':'text/plain'});
			//Ends the response process
			response.end('Data to be sent to the database!');


}

}).listen(4000);

console.log('Server running at localhost:4000');